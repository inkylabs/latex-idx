// - ek = entry key
// - en = entry name
// - gk = group key
// - gn = group name
// - p  = page label
import { promises as fs } from 'fs'

const re = new RegExp('^\\\\indexentry\\{' +
  '(' +
   '(?<gk>.*?)' +
   '@(?<gn>.*?)' +
  '!)?' +
  '(?<ek>.*?)' +
  '@(?<en>.*?)' +
  '\\|(?<opts>.*?)' +
  '\\}' +
  '\\{(?<p>\\w+)\\}$'
)

function toHtml (text) {
  if (!text) return text
  return text.replace(/\\textit *\{(.*?)\}/g, '<i>$1</i>')
}

async function read (filepath) {
  const data = await fs.readFile(filepath, 'utf8')
  const ret = {
    sequence: [],
    groups: []
  }
  ret.sequence = data
    .split(/\n/g)
    .filter(l => !!l)
    .map(l => l.match(re).groups)
    .map((g, i) => Object.assign(g, {
      id: i,
      // Plain formatting options should be counted as starts so that they
      // show up in the final list.
      start: g.opts !== ')',
      opts: ['(', ')'].includes(g.opts) ? null : g.opts
    }))

  if (!ret.sequence.length) return ret

  const entries = {}
  ret.sequence
    .forEach(e => {
      e.en = toHtml(e.en || e.ek)
      e.gn = toHtml(e.gn || e.gk)

      const key = `${e.gk}.${e.ek}`

      if (e.opts) {
        const m = e.opts.match(/^see(also)?\{(.*?)\}$/)
        if (m) {
          const s = m[1] === 'also' ? '<i>See also</i>' : '<i>See</i>'
          e.pp = `${s} ${m[2]}`
        }
        return
      }

      if (e.start) {
        if (key in entries) {
          console.error(`duplicate index start "${e.ek}"`)
          return
        }

        entries[key] = e
        return
      }

      const start = entries[key]
      if (!start) {
        console.error(`index entry not found "${e.ek}"`)
        return
      }
      delete entries[key]
      const sp = start.p
      const ep = e.p
      start.startPage = sp
      start.endPage = ep
      start.pp = sp === ep ? sp : `${sp}–${ep}`
    })
  const starts = ret.sequence.filter(e => e.start)
  const groups = {}
  starts.forEach(e => {
    groups[e.gk] = {
      key: e.gk,
      name: e.gn,
      entries: {}
    }
  })
  starts.forEach(e => {
    groups[e.gk].entries[e.ek] = {
      key: e.ek,
      name: e.en,
      instances: []
    }
  })
  starts.forEach(e => {
    const l = groups[e.gk].entries[e.ek].instances
    const last = l[l.length - 1] || {}
    if (last.startPage &&
      last.startPage === e.startPage &&
      last.endPage === e.endPage) {
      return
    }
    l.push(e)
  })

  ret.groups = Object.values(groups)
    .sort((a, b) => {
      a = (a.key || '').toLowerCase()
      b = (b.key || '').toLowerCase()
      if (a < b) return -1
      if (a > b) return 1
      return 0
    })
  ret.groups
    .forEach(g => {
      g.entries = Object.values(g.entries)
        .sort((a, b) => {
          a = (a.key || '').toLowerCase()
          b = (b.key || '').toLowerCase()

          if (a < b) return -1
          if (a > b) return 1
          return 0
        })
    })

  return ret
}

async function readConfig (config) {
  const idxs = {}
  for (const [k, v] of Object.entries(config)) {
    idxs[k] = await read(v.idx)
  }
  return idxs
}

export default {
  read,
  readConfig
}
